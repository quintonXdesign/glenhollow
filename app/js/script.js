/* Author: John Gibby @thatgibbyguy */

// ==========================
/* Store current location */
// ==========================

var pathName = location.pathname;

// ==========================
/* Remove Phone Link on Desktop */
// ==========================

remPhoneLink = function(){
	docWidth = $(document).width();
	if (docWidth >= 1024){
		$('[data-query="phone"]').click(function(){
			return false;
		});
	}
	else{
		$('[data-query="phone"]').click(function(){
			return true;
		});
	}
};
remPhoneLink();

// ==========================
/* Adding and removing classes */
// ==========================

var addClassTo = function(query){
	$('[data-query="' + query + '"]').addClass('on');
};
var removeClassFrom = function(query){
	$('[data-query="' + query + '"]').removeClass('on');
};
var addClassByUrl = function(path,query){
	if(pathName.indexOf(path) >= 0){
		$('[data-query="' + query + '"]').addClass('on');
	}
}; 

// ==========================
/* Mobile Menu Trigger */
// ==========================

var nav = '';
var toggler = document.getElementById('mobile-toggle');

$(toggler).click(function(){
	$(nav).toggleClass('mobile-hidden').toggleClass('show');
});


// ==========================
/* Tabbed Navigation */
// ==========================

$('.main-nav .tabs li').click(function(){
	var tab_id = $(this).attr('data-tab');
	$('article.tab-content').fadeOut(150);
	$('article#'+tab_id).delay(150).fadeIn(150);
});

$('#amesbury-cares').click(function(){
	$('article.tab-content').fadeOut(150);
	$('article#tab-7').delay(150).fadeIn(150);
});
$('#contact').click(function(){
	$('article.tab-content').fadeOut(150);
	$('article#tab-6').delay(150).fadeIn(150);
});
$('#gallery').click(function(){
	$('article.tab-content').fadeOut(150);
	$('article#photo-gallery').delay(150).fadeIn(150);
});


// ==========================
/* Close-x function*/
// ==========================

$('.close-x').click(function(){
	$(this).parent().fadeOut(150);
});


// ----------------------------
/* Accordions*/
// ----------------------------
$('.accordion').click(function () {
	var $this = $(this);
	$this.next().slideToggle('200');
	 $this.find('i').toggleClass('down');
});
  
$('ul.floorplans a').click(function(){
	console.log('click');
	var link_id = $(this).attr('data-link');
	$('#tab-2 .floorplan-photo').slideUp(150);
	$('[data-link="'+link_id+'"]').delay(150).slideDown(150);
});


// ----------------------------
/* maximage*/
// ----------------------------

$(function(){
	
		$('#maximage').maximage({
	        cycleOptions: {
	            speed: 1200,
	            timeout: 6000,
	        },
	        onImagesLoaded: function(){
	        	$('#maximage').fadeIn();
	        }
	    });
	
});


